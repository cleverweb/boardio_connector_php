<?php

namespace Perfico\Exporter;

use Perfico\Connection\TransportConfig;

interface ConfigAware
{
    public function setConfig(TransportConfig $config);
}