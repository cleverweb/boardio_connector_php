<?php
/**
 * Created by IntelliJ IDEA.
 * User: si
 * Date: 15/12/15
 * Time: 11:42
 */

namespace Perfico\Exporter;


interface BoardioExporter
{
    public function after();

    public function before();

    public function run();
}