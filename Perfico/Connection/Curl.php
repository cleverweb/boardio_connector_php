<?php
namespace Perfico\Connection;

use stdClass;

class Curl
{
    private $url;
    public $curlError;

    public function __construct($host)
    {
        $this->host = $host;
        $this->url = $host;
    }

    /**
     * Определяет URL для curl запроса
     *
     * @param $api_token
     * @param $key
     * @param $url
     *
     */
    private function  setUrl($url, $api_token, $key)
    {
        if (isset($api_token)) {
            $this->url = "$url/api/push/$api_token/$key";
        } else {
            $this->url = "$url/api/simpleWidgets";
        }
    }

    /**
     * Получает параметры для запроса и возвращает объект с ответом от сервера
     * или с ошибками curl
     *
     * @param object $widget
     * @param string $method
     *
     * @return stdClass
     */
    public function request($jsonData, $token)
    {
        $method = 'POST';


        $headers = array(
            "Content-Type: application/json; charset=utf-8; Accept: */*; Cache-Control: no-cache",
            "Authorization: Bearer $token"
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);

        $httpResponse = curl_exec($ch);
        $httpError = curl_error($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);


        if($httpError) {
            throw new CurlException($httpError);
        }

        if($info['http_code'] != 200) {
            throw new CurlException($httpResponse);
        }

        $response = new Response($info);
        $response->setRequestBody($jsonData);

        return $response;

//        var_dump($info);
//        var_dump($httpError);
//
//        return $response;
    }
}