<?php
/**
 * Created by IntelliJ IDEA.
 * User: si
 * Date: 13/12/15
 * Time: 20:22
 */

namespace Perfico\Connection;


class Response
{
    /**
     * @var array
     */
    protected $curlResponse;


    protected $requestBody;

    /**
     * Response constructor.
     * @param int $httpStatus
     * @param bool $successful
     */
    public function __construct($curlResponse)
    {
        $this->curlResponse = $curlResponse;
    }

    /**
     * @return mixed
     */
    public function getRequestBody()
    {
        return $this->requestBody;
    }

    /**
     * @param mixed $requestBody
     */
    public function setRequestBody($requestBody)
    {
        $this->requestBody = $requestBody;
    }

    /**
     * @return boolean
     */
    public function isSuccessful()
    {
        return $this->curlResponse['http_code'] == 200;
    }
}