<?php

namespace Perfico\Connection;

class GenericConnector
{
    /**
     * @var TransportConfig
     */
    protected $config;

    public function __construct(TransportConfig $config) {

        $this->config = $config;

        $this->transport = new Transport($this->config->apiEndPoint, $this->config->token);
    }
}