<?php
namespace Perfico\Connection;

use Perfico\Connection\Response;
use Perfico\Exporter\EmptyDatasourcesException;

class Transport
{
    public function __construct($host, $accessToken)
    {
        $this->curl = new Curl($host);
        $this->host = $host;
        $this->token = $accessToken;
    }

    /**
     * @param DataSource | DataSource[] $data
     *
     * @throws EmptyDatasourcesException
     * @return Perfico\Connection\Response[]
     */
    public function send($data)
    {
        if(!$data) {

            throw new EmptyDatasourcesException();
        }

        $results = [];
        if(is_array($data)) {
            foreach($data as $dataSource) {

                /** @var DataSource $dataSource */
                $results[$dataSource->hash] = $this->curl->request($dataSource->toJson(), $this->token);
            }
        } else {

            $results[$data->hash] = $this->curl->request($data->toJson(), $this->token);
        }

        return $results;
    }

}