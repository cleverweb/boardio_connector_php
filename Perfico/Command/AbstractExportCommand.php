<?php

namespace Perfico\Command;

use Perfico\Connection\Response;
use Perfico\Exporter\ConfigAware;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;


abstract class AbstractExportCommand extends Command
{

    protected function printResult(OutputInterface $output, $result) {


        /** @var Table $table */
        $table = $this->getHelper("table");
        $table->setHeaders(["DataSource", "Status"]);


        foreach($result as $key => $item) {

            /** @var Response $item */
            $table->addRow([
                $key,
                ( $item->isSuccessful()?"OK":"Error")
            ]);
        }


        $table->render($output);
    }

    protected function printFullResult(OutputInterface $output, $result) {


        /** @var Table $table */
        $table = $this->getHelper("table");
        $table->setHeaders(["DataSource", "Status", "Request"]);


        foreach($result as $key => $item) {

            /** @var Response $item */
            $table->addRow([
                $key,
                ( $item->isSuccessful()?"OK":"Error"),
                $item->getRequestBody()
            ]);
        }


        $table->render($output);
    }


    protected function injectConfig($object) {

        if($object instanceof ConfigAware) {
            $object->setConfig($this->config);
        }
    }
}