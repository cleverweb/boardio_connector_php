<?php

namespace Perfico\Command;

use Perfico\Connection\GenericConnector;
use Perfico\Connection\Response;
use Perfico\Connection\TransportConfig;
use Perfico\Exporter\ConfigAware;
use Perfico\Exporter\EmptyDatasourcesException;
use Perfico\Model\DataSource;
use Perfico\Model\SourceValue;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadSingleClass extends AbstractExportCommand
{
    protected $config;

    public function setConfig(TransportConfig $config) {

        $this->config = $config;
    }

    protected function configure()
    {
        $this
            ->setName('export:class')
            ->setDescription('Import datasource from single class')
            ->addArgument('class', InputArgument::REQUIRED,
                'Please, specify exporting PHP class'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $genericConnector = new GenericConnector($this->config);

        $fullClass = $input->getArgument('class');

        $exporter = new $fullClass();

        $this->injectConfig($exporter);

        $exporter->before();
        $exportResult = $exporter->run();
        $exporter->after();

        try {
            $result = $genericConnector->transport->send($exportResult);


            $output->writeln("\n \n Export report");

            if($output->getVerbosity() > OutputInterface::VERBOSITY_VERBOSE) {
                $this->printFullResult($output, $result);
            } else {
                $this->printResult($output, $result);
            }

        } catch( EmptyDatasourcesException $e) {

            $output->writeln("<error>\n \n No datasources found \n</error>" );
        }
    }


}