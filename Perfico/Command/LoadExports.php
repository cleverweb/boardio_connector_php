<?php

namespace Perfico\Command;

use Perfico\Connection\GenericConnector;
use Perfico\Connection\Response;
use Perfico\Connection\TransportConfig;
use Perfico\Exporter\ConfigAware;
use Perfico\Model\DataSource;
use Perfico\Model\SourceValue;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadExports extends AbstractExportCommand
{
    protected $config;

    public function setConfig(TransportConfig $config) {

        $this->config = $config;
    }

    protected function configure()
    {
        $this
            ->setName('export:namespace')
            ->setDescription('Parse export classes from some namespace')
            ->addArgument("namespace", InputArgument::REQUIRED, 'Please, specify exporting classes namespace')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $genericConnector = new GenericConnector($this->config);

        $classes = $this->scanDir(
            $this->convertScopeToPath($input->getArgument('namespace'))
        );

        /** @var ProgressBar $progress */
        $progress = $this->initializeProgressBar($output, count($classes));

        $dataSources = [];

        $result = [];
        foreach($classes as $class) {


            try {
                $this->processClass($input, $class, $dataSources, $progress, $result, $genericConnector);
            } catch( EmptyDatasourcesException $e) {

                $output->writeln("<error>\n \n No datasources found \n</error>" );
            }
        }

        if(!$result) {
            $output->writeln("<error>\n \n No datasources found \n</error>" );
        } else {

            $output->writeln("\n \n Export report");

            if($output->getVerbosity() > OutputInterface::VERBOSITY_VERBOSE) {
                $this->printFullResult($output, $result);
            } else {
                $this->printResult($output, $result);
            }
        }

    }

    protected function scanDir($dir) {

        $exporters = [];
        foreach (scandir($dir) as $file) {

            $exporter = $this->cleaUpFileName($file);

            if($exporter) {
                $exporters[] = $exporter;
            }

        }

        return $exporters;
    }

    protected function cleaUpFileName($filename) {

        if($filename != '.' && $filename != '..' && strpos($filename, ".php") ) {
            return str_replace('.php', '', $filename);
        }
        return null;
    }

    protected function initializeProgressBar($output, $count) {

        // create a new progress bar (50 units)
        $progress = new ProgressBar($output, $count);

        // start and displays the progress bar
        $progress->start();
        $progress->setFormat(' %current%/%max% [%bar%] %percent:3s%% %message%');

        return $progress;
    }

    protected function convertScopeToPath($scope) {
        return 'src/' . str_replace("\\", "/", $scope);
    }

    protected function convertScopeToClassPrefix($scope) {
        return $scope;
    }

    /**
     * @param InputInterface $input
     * @param $class
     * @param $dataSources
     * @param $progress
     * @param $result
     * @param $genericConnector
     * @return array
     */
    protected function processClass(InputInterface $input, $class, $dataSources, $progress, &$result, $genericConnector)
    {
        $fullClass = $this->convertScopeToClassPrefix($input->getArgument('namespace')) . '\\' . $class;

        $exporter = new $fullClass();

        $this->injectConfig($exporter);

        $exporter->before();

        $exportResult = $exporter->run();

        $exporter->after();

        if(!$exportResult) {
            return null;
        }

        if (is_array($exportResult)) {
            $dataSources = array_merge($dataSources, $exportResult);
        } else {
            $dataSources[] = $exportResult;
        }


        foreach ($dataSources as $dataSource) {

            $progress->setMessage($dataSource->name);
            $progress->display();

            $result = array_merge($result, $genericConnector->transport->send($dataSource));
        }

        $progress->advance();

    }

}