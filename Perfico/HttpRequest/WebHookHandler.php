<?php

namespace Perfico\HttpRequest;

use Perfico\Connection\GenericConnector;
use Perfico\Connection\TransportConfig;
use Perfico\Exporter\BoardioExporter;
use Perfico\Exporter\ConfigAware;

class WebHookHandler
{
    /**
     * @param BoardioExporter $exporter
     * @param TransportConfig $config
     */
    public static function process(BoardioExporter $exporter, TransportConfig $config) {


        $genericConnector = new GenericConnector($config);

        self::injectConfig($exporter, $config);

        $exporter->before();
        $exportResult = $exporter->run();
        $exporter->after();

        return $genericConnector->transport->send($exportResult);
    }

    /**
     * @param BoardioExporter $exporter
     * @param TransportConfig $config
     */
    protected static function injectConfig(BoardioExporter $exporter, TransportConfig $config)
    {
        if ($exporter instanceof ConfigAware) {
            $exporter->setConfig($config);
        }
    }

}