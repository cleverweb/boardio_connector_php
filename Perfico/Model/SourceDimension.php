<?php

namespace Perfico\Model;


class SourceDimension
{
    public $key;

    public $label;

    /**
     * SourceDimension constructor.
     * @param $key
     * @param $label
     */
    public function __construct($key, $label)
    {
        $this->key = $key;
        $this->label = $label;
    }


}