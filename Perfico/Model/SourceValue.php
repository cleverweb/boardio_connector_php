<?php

namespace Perfico\Model;

class SourceValue
{
    public $current;

    public $previous;

    public $suffix = null;

    public $prefix = null;

    public $name = null;

    public $url = null;

    public $dimensions = null;

    function __construct() {
    }

    public function addDimension($key, $value) {
        $this->dimensions[$key] = $value;
    }
}