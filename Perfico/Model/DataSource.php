<?php
/**
 * Created by IntelliJ IDEA.
 * User: si
 * Date: 16/11/15
 * Time: 17:59
 */

namespace Perfico\Model;


class DataSource
{

    public $hash;

    public $name;

    public $sourceDimensions = [];

    public $sourceValues = [];

    function __construct($hash) {
        $this->hash = $hash;
    }

    public function toJson() {
        return json_encode($this,  JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT );
    }

    public function addSourceValue(SourceValue $sourceValue) {
        $this->sourceValues[] = $sourceValue;
    }

    public function addSourceDimension(DataSourceDimension $dataSourceDimension) {
        $this->sourceDimensions[] = $dataSourceDimension;
    }
}