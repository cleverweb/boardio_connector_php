#CleverWeb PHP boardio connector
 
##Links

1. [stage-boardio.ru](http://www.stage-boardio.ru)
2. [Dashboards](https://dasho.clever-web.net/)
3. [app](https://bitbucket.org/cleverweb/boardio_connector_php)

##Geting started

###Installation

- Add `"perfico/boardio-connector": "dev-master"` in module `require` 
and `"git@bitbucket.org:cleverweb/boardio_connector_php.git"` in module `repositories` 
to composer.json of your application. Or clone repo to your project.
- If you are using composer - simply use `require_once 'vendor/autoload.php';` otherwise paste following code:
```php
require_once('./lib/SimpleWidget.php');
require_once('./lib/Transport.php');
require_once('./lib/Curl.php');
```
###Using
- Include use classes

```php
//including files 
use Perfico\Dashboard\Transport;
use Perfico\Dashboard\SimpleWidget;
```

- Insert your data in variables:

```php
$apiToken = "4764d3d30ed0bca68cecbcf37fe260a4"; // Your auth token
$key = "9f8af0f2cb23387a56d67597b0961d67"; // Widget key 
$current = "5432"; // current value
$previous = "1000"; // previous value
$suffix = " rub"; // suffix 
$prefix = "$"; // prefix
$url = "https://dashboards.clever-web.net"; //url (not required)
```

- And call method `send()` :

```php
//call class
$transport = new Transport();
$result = $transport->send(new Dashboard\SimpleWidget(
    $apiToken, $key, $current, $previous, $suffix, $prefix
));
return new JsonResponse($result);
```