<?php
namespace tests;
//require '../vendor/autoload.php';


use Dashboard\SimpleWidgetPusher;
use Perfico\Dashboard\DataSourceDimension;
use Perfico\Dashboard\SourceDimension;
use Perfico\Dashboard\SourceValue;
use Perfico\Dashboard\DataSource;

class SimpleWidgetPushTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function SourceValueClassExists() {
        $sourceValue = new SourceValue();
        $this->assertTrue($sourceValue instanceof SourceValue);
    }

    /**
     * @test
     */
    public function SourceValueInDataSource() {
        $sourceValue = new SourceValue();
        $sourceValue->current = 10;
        $sourceValue->previous = 5;
        $sourceValue->name = "Net revenue";
        $sourceValue->url = "http://test.com/1.html";
        $sourceValue->suffix = "h";

        $dataSource = new DataSource("unique-hash-1");
        $dataSource->name = "Graph data";
        $dataSource->addSourceValue($sourceValue);

        $this->assertJson($dataSource->toJson());
        $this->assertJsonStringEqualsJsonFile("tests/examples/data_source1.json",  $dataSource->toJson());
    }

    /**
     * @test
     */
    public function SourceValueWithDimensionInDataSource() {

        $dataSource = new DataSource("shipping-delays");
        $dataSource->name = "Shipping Delays";
        $dataSource->addSourceDimension(new DataSourceDimension("vessel", "Vessel name"));
        $dataSource->addSourceDimension(new DataSourceDimension("reason", "Delay reason"));


        $sourceValue = new SourceValue();
        $sourceValue->current = 90;
        $sourceValue->previous = null;
        $sourceValue->name = null;
        $sourceValue->url = null;
        $sourceValue->suffix = "h";
        $sourceValue->addDimension("vessel", "Omsky-109");
        $sourceValue->addDimension("reason", "Задержка в порту");
        $dataSource->addSourceValue($sourceValue);


        $sourceValue = new SourceValue();
        $sourceValue->current = 15;
        $sourceValue->previous = null;
        $sourceValue->name = null;
        $sourceValue->url = null;
        $sourceValue->suffix = "h";
        $sourceValue->addDimension("vessel", "Omsky-102");
        $sourceValue->addDimension("reason", "Задержка в порту");
        $dataSource->addSourceValue($sourceValue);

        $this->assertJson($dataSource->toJson());
        $this->assertJsonStringEqualsJsonFile("tests/examples/data_source2.json",  $dataSource->toJson());
    }
}